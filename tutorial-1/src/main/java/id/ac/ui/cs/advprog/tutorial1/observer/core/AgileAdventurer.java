package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
        //ToDo: Complete Me
        this.guild.add(this);
    }

    //ToDo: Complete Me

    public void update(){
        if((this.guild.getQuestType().equals("D"))||(this.guild.getQuestType().equals("R"))){
            this.getQuests().add(this.guild.getQuest());
        }
    }


}
