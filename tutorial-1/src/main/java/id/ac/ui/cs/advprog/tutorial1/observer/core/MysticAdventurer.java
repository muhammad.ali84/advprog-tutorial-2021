package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {

    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
        this.guild.add(this);

        //ToDo: Complete Me
    }

    public void update(){
        if((this.guild.getQuestType().equals("D"))||(this.guild.getQuestType().equals("E"))){
            this.getQuests().add(this.guild.getQuest());
        }
    }


    //ToDo: Complete Me
}
